import logging
import logging.config
import os

import yaml

from biz.login_biz import LoginBiz

# Create a logger
logger = logging.getLogger(__name__)


def conf_log():
    log_filename = "logs/crawl.log"
    os.makedirs(os.path.dirname(log_filename), exist_ok=True)

    with open("configs/logging.yaml", "r") as f:
        yaml_config = yaml.safe_load(f.read())
        logging.config.dictConfig(yaml_config)


def login_session():
    LoginBiz()


def main():
    conf_log()
    login_session()


if __name__ == "__main__":
    main()
