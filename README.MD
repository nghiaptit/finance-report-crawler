# BCTC Crawler using Python Selenium

## Table Of Contents

* [Pre-requisites](#pre-requisites)
* [Run Your First Test](#run-your-first-test)
* [Local Testing With Python](#testing-locally-hosted-or-privately-hosted-projects)

## Prerequisites

Before you can start performing **Python** automation crawl with **Selenium**, you would need to:

* Install the latest Python build from the [official website](https://www.python.org/downloads/). We recommend using the
  latest version.
* Make sure **pip** is installed in your system. You can install **pip**
  from [here](https://pip.pypa.io/en/stable/installation/).
* Download the latest **Selenium Client** and its **WebDriver bindings** from
  the [official website](https://sites.google.com/chromium.org/driver/downloads?authuser=0).

### Installing Dependencies and Environment

**Step 1:** Clone the repository and navigate to the code directory as shown below:

```bash
git clone https://gitlab.com/nghiaptit/finance-report-crawler.git
cd finance-report-crawler
```

**Step 2:** Download the driver which matches the **version of Chrome** installed on **your machine**. Then, copy bin
file to **bin/driver**

**Step 3:** Install virtual environment in python and activate them

```bash
cd finance-report-crawler
python3 -m venv venv
```

On Mac

```bash
source venv/bin/activate
```

On Windows

```Windows
venv\Script\activate
```

**Step 4:** Install whole dependencies(libraries) is defined in requirements.txt files.

```bash
pip install -r requirements.txt
```

## Run Your First Test

### Configuration Of Your Credential

**Step 5:** Go to the root folder & input your credential to login. If success, cookies will be saved

```bash
python3 login.py
```

**Auto mode**: Turn on auto in config file. The program will get credential in config file to input and login.

**Manual mode**: Set auto in config file is off. The program will wait to login in **{wait_time}** second, which defined in
config file

**Step 6:** Start crawl

```bash
python3 main.py
```
